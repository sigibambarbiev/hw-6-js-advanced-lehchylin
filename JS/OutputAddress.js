import {button} from "./Variables.js";

export function outputAddress(address) {

    const list = document.createElement('ul');
    button.after(list);

    for (const objKey in address) {

        const info = document.createElement('li');
        list.append(info);

        (!!address[objKey] && (info.innerText = `${objKey}: ${address[objKey]}`)) || (info.innerText = `${objKey}: unknown`);

    }
}