
export async function getAddress(queryClientIp, queryIpService) {

    const clientIP = await fetch(queryClientIp, {method: 'GET'});
    const clientIpJson = await clientIP.json();

    const clientAddress = await fetch(`${queryIpService}/json/${clientIpJson.ip}?fields=continent,country,regionName,city,district`, {method: 'GET'});
    const clientAddressJson = await clientAddress.json();

    return clientAddressJson;
}

