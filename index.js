import {getAddress} from "./JS/GetAddress.js";
import {outputAddress} from "./JS/OutputAddress.js";
import {URL_QUERY_CLIENT_IP, URL_QUERY_IP_SERVICE, button} from "./JS/Variables.js";


button.addEventListener('click',  async e => {

    await outputAddress(await getAddress(URL_QUERY_CLIENT_IP, URL_QUERY_IP_SERVICE));

});

